
// clear any placeholder data before load
if (firstLoad) {
	dataSheet.items = [];
}

const fetchComplete = {{{LOADCOMPLETE_FUNCDECL}}}

const url = dataSheet.urlFromOptions(options);  // this method was written by the web service plugin

const fetchOpts = {
  method: '{{{HTTPMETHOD}}}',
  headers: {{{CUSTOMHEADERSJSON}}},
};

fetch(url, fetchOpts)
  .then((response) => {
    if (response.status >= 400) {
      throw new Error("Server error: "+response.status);
    }
    return response.json();
  })
  .then((json) => {
    dataSheet.loadFromJson(json);
    fetchComplete(null, options);
  })
  .catch((exc) => {
    fetchComplete(exc, options);
  });
