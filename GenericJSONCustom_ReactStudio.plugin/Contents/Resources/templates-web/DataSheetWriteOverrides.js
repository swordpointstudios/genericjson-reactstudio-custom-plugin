
urlFromOptions(options) {
  const baseUrl = "{{{URLBASE}}}";
  
  let path = options.servicePath || '';
  if (path.length > 0 && path.substr(0, 1) !== '/' && baseUrl.substr(baseUrl.length - 1, 1) !== '/')
    path = '/'+path;

  let query = options.query || '';
  if (query.length > 0) {
    const dataSlots = options.dataSlots || {};
    query = "?" + this.expandSlotTemplateString(query, dataSlots);
  }

  return baseUrl + path + query; 
}

// this function's implementation is provided by React Studio.
_fetchComplete = {{{WRITECOMPLETE_FUNCDECL}}}

addItem(item, options) {
  super.addItem(item, options);
  
  const url = this.urlFromOptions(options);
  
  let headers = {{{CUSTOMHEADERSJSON}}};
  headers['Content-Type'] = 'application/json';

  const fetchOpts = {
    method: 'POST',
  	headers: headers,
  	body: JSON.stringify(item),
  };

  fetch(url, fetchOpts)
    .then((response) => {
      if (response.status >= 400) {
        console.log("** Error sending %s to %s, response: ", fetchOpts.method, url, response);
        throw new Error("Server error: "+response.status);
      }
      return response.json();
    })
    .then((json) => {
      this._fetchComplete(null, options);
    })
    .catch((exc) => {
      this._fetchComplete(exc, options);
    });  
}

removeItem(item, options) {
  super.removeItem(item, options);
  
  const url = this.urlFromOptions(options);
  
  let headers = {{{CUSTOMHEADERSJSON}}};
  headers['Content-Type'] = 'application/json';

  const fetchOpts = {
    method: 'DELETE',
  	headers: headers,
  	body: JSON.stringify(item),
  };

  fetch(url, fetchOpts)
    .then((response) => {
      if (response.status >= 400) {
        console.log("** Error sending %s to %s, response: ", fetchOpts.method, url, response);
        throw new Error("Server error: "+response.status);
      }
      return response.json();
    })
    .then((json) => {
      this._fetchComplete(null, options);
    })
    .catch((exc) => {
      this._fetchComplete(exc, options);
    });  
}

replaceItemByRowIndex(idx, item, options) {
  super.replaceItemByRowIndex(idx, item, options);
  
  const url = this.urlFromOptions(options);
  
  let headers = {{{CUSTOMHEADERSJSON}}};
  headers['Content-Type'] = 'application/json';

  const fetchOpts = {
    method: 'PUT',
  	headers: headers,
  	body: JSON.stringify(item),
  };

  fetch(url, fetchOpts)
    .then((response) => {
      if (response.status >= 400) {
        console.log("** Error sending %s to %s, response: ", fetchOpts.method, url, response);
        throw new Error("Server error: "+response.status);
      }
      return response.json();
    })
    .then((json) => {
      this._fetchComplete(null, options);
    })
    .catch((exc) => {
      this._fetchComplete(exc, options);
    });  
}

