
// clear previous data before load
if (firstLoad) {
	dataSheet.items = [];
}

const fetchCompleteFunc = {{{DATALOADED}}}

const baseUrl = "{{{URLBASE}}}";
let path = servicePath;
if (path.length > 0 && path.substr(0, 1) !== '/' && baseUrl.substr(baseUrl.length - 1, 1) !== '/')
  path = '/'+path;

const url = baseUrl + path;

const fetchOpts = {
  method: '{{{HTTPMETHOD}}}',
  headers: {{{CUSTOMHEADERSJSON}}},
};

fetch(url, fetchOpts)
  .then(function(response) {
    if (response.status >= 400) {
      throw new Error("Server error: "+response.status);
    }
    return response.json();
  })
  .then(function(json) {
    if ( !Array.isArray(json)) {
			throw new Error("Invalid data from server (not an array)");
		}

		let items = [];
		for (var i = 0; i < json.length; i++) {
			let row = json[i];
			let item = {};
			item.key = row.id || i;
			for (var col in row) {
				if (row.hasOwnProperty(col)) {
					var newCol = col;
					item[newCol] = row[col];
				}
			}
			items.push(item);
		}
		
    dataSheet.items = items;
    
    fetchCompleteFunc(null);
  })
  .catch(function(exc) {
    fetchCompleteFunc(exc);
  });
