/*
  - 2017.01.15 / Pauli Ojala / pauli@neonto.com
*/


// -- plugin info requested by host app --

this.pluginAllowsMultipleInstances = true;

this.describePlugin = function(id, lang) {
  switch (id) {
    case 'displayName':
      return "Generic JSON Custom";

    case 'shortDisplayText':
      return "Fetch and update data from a web service using the JSON format.";

    case 'defaultNameForNewInstance':
      return "json-src";

    case 'uiTexts':
      return {
        "label:sheetEditor:servicePathHeader": "URL path",
        "label:sheetEditor:servicePathInfo": "Path to the resource.",
      }
  }
}


// -- entry point called by React Studio to fill out a data sheet --

this.readDataSheetFromServiceAsync = function(httpClient, sheetOptions, finalCb) {
  var baseUrl = this._data.url;
  if ( !baseUrl || baseUrl.length < 1) {
    finalCb(new Error("Please specify a URL for the Generic JSON source plugin."), null);
    return;
  }
  var path = sheetOptions.path || "";
  
  if (path.length > 0 && path.substr(0, 1) !== '/' && baseUrl.substr(baseUrl.length - 1, 1) !== '/')
    path = '/'+path;

  var url = baseUrl + path;
  console.log("reading from url "+url);
  
  var headers = {};
  if (this._data.customHeaders && this._data.customHeaders.trim().length > 0) {
    try {
      headers = JSON.parse(this._data.customHeaders);
    } catch (e) {
      finalCb(new Error("Custom headers are not valid JSON."), null);
      return;
    }    
  }

  const httpMethod = this._data.httpMethod == 1 ? "POST" : this._data.httpMethod == 2 ? "DELETE" : this._data.httpMethod == 3 ? "PATCH" : "GET"

  var self = this;
  httpClient.load(url, {encoding: 'utf8', method: httpMethod, headers: headers,body:sheetOptions.query}, function(err, httpStatus, data) {
    var parsedRows = null;
    if ( !err) {
      try {
        var obj = JSON.parse(data);
        parsedRows = self.parseSheetRows(obj, sheetOptions);
      } catch (exc) {
        err = new Error("Error loading data from API: "+exc);
      }
    }
    finalCb(err, parsedRows);
  });
}

this.parseSheetRows = function(data, opts) {
  if ( !data) {
    throw new Error("No data from server.");
  }
  if ( !Array.isArray(data)) {
    var keyPath = opts.requestedKeyPath;
    if ( !keyPath || keyPath.length < 1) {
      data = [data];
    } else {
      // look for the requested key path within this object
      var pathComps = keyPath.split('.');
      for (var comp of pathComps) {
        if ( !data.hasOwnProperty(comp)) break;
        data = data[comp];
      }
      if ( !data.length) {
        data = [data];
      }
    }
  }

  var imgCols = this._parseImageColumnEntries();

  var srcRows = data;
  var sheetRows = [];
  for (var i = 0; i < srcRows.length; i++) {
    var row = srcRows[i];
    var sheetRow = {};

    for (var key in row) {
      var val = row[key];
      if (Array.isArray(val) || typeof val === 'object') {
        sheetRow[key] = {
          type: 'json',
          value: JSON.stringify(val)
        };
      } else if (imgCols.indexOf(key) != -1) {
        sheetRow[key] = {
          type: 'image',
          value: val
        };
      } else {
        sheetRow[key] = {
          type: 'text',
          value: val
        };
      }
    }
    sheetRows.push(sheetRow);
  }
  return sheetRows;
}

this._parseImageColumnEntries = function() {
  var imgCols = [];
  if (this._data.imageColumns != null) {
    var entries = this._data.imageColumns.split(',');
    for (var key in entries) {
      imgCols.push(entries[key].trim());
    }
  }
  return imgCols;
}

this._writeStringArray = function(entries, template) {
  var str = "";
  var prefix = "";
  for (var key in entries) {
    var view = {
      "ENTRY": entries[key],
    };
    str += prefix+this.Mustache.render(template, view);
    prefix = ", "
  }
  return str;
}



// -- code generation, React web --

this.getReactWebImports = function() {
  // This plugin uses the 'isomorphic-fetch' library to load data from server in the generated code.
  // "Isomorphic-fetch" is a wrapper to provide a cross-browser implementation of the HTML5 Fetch API.
  // It's available in the default create-react-app package, so we just need to import it.
  // (If the library were not available, we'd have to include it in resources and declare it in our plugin's Info.plist)
  return [
    "fetch from 'isomorphic-fetch'",
  ];
}

// This property tells the host app that we want to get regular updates from the server.
this.dataSheetUpdateInterval_ReactWeb = 10.0;

this._getCustomHeadersString = function() {
  var customHeaders = (this._data.customHeaders || '').trim();
  if (customHeaders.length < 2) {
    customHeaders = "{}";
  }
  return customHeaders;
}

this._getHttpMetthod = function () {
 return this._data.httpMethod == 1 ? "POST" : this._data.httpMethod == 2 ? "DELETE" : this._data.httpMethod == 3 ? "PATCH" : "GET"
}

this.writeReactWebCodeForDataSheetInit = function(exporter, sheetId) {
  return `this.dataSheets['${sheetId}'].dataSlots = this.dataSlots;\n`;
}

this.writeReactWebCodeForDataSheetLoad = function(exporter, loadCompleteCode)
{
  // The code returned from this function will become the body for a function with arguments:
  //   (dataSheet, options)
  //
  // 'loadCompleteCode' is a function definition of style:  (err, options) => { ... }
  // It must be called when the load has completed.
  //
  // The 'options' value should be passed through to when calling the function of 'loadCompleteCode'.
  // It contains properties like 'servicePath' and 'query' (and possibly others related to the operation).
    
  var template = Plugin.readResourceFile("templates-web/DataSheetLoad.js", 'utf8');
  var view = {
    "LOADCOMPLETE_FUNCDECL": loadCompleteCode,
    "HTTPMETHOD": this._getHttpMetthod(),
    "CUSTOMHEADERSJSON": this._getCustomHeadersString(),
    "IMAGECOLUMNS": this._writeStringArray(this._parseImageColumnEntries(), '"{{ENTRY}}"'),
  }
  var code = this.Mustache.render(template, view);
  return code;
}

this.writeReactWebCodeForDataSheetWriteOverrides = function(exporter, writeCompleteCode) {
  // The code returned from this function will be placed in a DataSheetBase subclass.
  //
  // 'writeCompleteCode' is a function definition of style:  (err, options) => { ... }
  // It must be called when a write has completed.
  // All overridden methods that modify data sheet items should call this when they're done.
  // These are: addItem, removeItem, replaceItemByRowIndex (or replaceItemByKey if you implement that instead).
  //
  // The 'options' value is provided as the last argument to those methods and should be passed through.
  // It contains properties like 'servicePath' and 'query' (and possibly others related to transactions).
	
  var template = Plugin.readResourceFile("templates-web/DataSheetWriteOverrides.js", 'utf8');
  var view = {
    "WRITECOMPLETE_FUNCDECL": writeCompleteCode,
    "URLBASE": this._data.url,
    "CUSTOMHEADERSJSON": this._getCustomHeadersString(),
  }
  var code = this.Mustache.render(template, view);
  return code;
}

this.writeReactWebCodeForSend = function(methodName, exporter, servicePath, instanceId) {
  var idata = this._data.instanceData.send[instanceId];
  return `
const expandSlotTemplateString = this.props.appActions.dataSheets['localizationSheet'].expandSlotTemplateString;

const url = expandSlotTemplateString(\`${this._data.url}${idata.apiEndpoint || ""}\`, this.props.appActions.dataSlots);
  
let headers = {
  'Content-Type': 'application/json'
};

const fetchOpts = {
  method: "${idata.httpMethod == 1 ? "POST" : idata.httpMethod == 2 ? "DELETE" : idata.httpMethod == 3 ? "PATCH" : "GET"}",
  headers: headers,
  ${idata.httpMethod != undefined && idata.httpMethod != 0 /* GET */ && idata.httpBody && idata.httpBody.length > 0 ? `
  body: JSON.stringify(${idata.httpBody}),
` : ``}
};

return fetch(url, fetchOpts)
  .then((response) => {
    if (response.status >= 400) {
      throw new Error("Server error: "+response.status);
    }
    return response.json();
  })
  .then((json) => {
    ${(idata.responseDataSlot != null && idata.responseDataSlot.length > 0) ?
      `(${exporter.setValueCodeForDataSlotByName(idata.responseDataSlot, '')})(json);` : ``}
  })
  .catch((exc) => {
    console.error(exc.message);
  });
`;
}



// -- code generation, AWS Lambda Node.js --

this.getAWSLambdaNodeImports = function() {
  return [
    "fetch = require('node-fetch')",
  ];
}

this.getAWSLambdaNodePackages = function() {
  return {
    "node-fetch": "^1.6.3"
  };
}

this.writeAWSLambdaNodeCodeForDataSheetLoad = function(exporter, loadCompleteCode) {
  // The returned code will be the body for a function with arguments:
  //   (dataSheet, servicePath)
  //
  // 'loadCompleteCode' is a function definition of style:  (err) => { ... }
  // It must be called when the load has completed.
  
  var customHeaders = this._data.customHeaders.trim();
  if (customHeaders.length < 2) {
    customHeaders = "{}";
  }
  
  var template = Plugin.readResourceFile("templates-lambda/DataSheetLoad.js", 'utf8');
  var view = {
    "DATALOADED": loadCompleteCode,
    "URLBASE": this._data.url,
    "HTTPMETHOD": this._getHttpMetthod(),
    "CUSTOMHEADERSJSON": customHeaders,
    "IMAGECOLUMNS": this._writeStringArray(this._parseImageColumnEntries(), '"{{ENTRY}}"'),
  }
  var code = this.Mustache.render(template, view);
  return code;
}

