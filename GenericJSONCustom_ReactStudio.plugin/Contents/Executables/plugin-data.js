// -- private variables --

this._data = {
  url: "https://api.example.com",
  imageColumns: "",
  customHeaders: "",
  httpMethod:0,
  allowInsecureHttp: false,
  updateInterval: 10.0,
  instanceData: {
		"send": {}
	}
};


// -- persistence, i.e. saving and loading --

this.persist = function() {
  return this._data;
}

this.unpersist = function(data) {
  if (data.instanceData === undefined) {
    data.instanceData = {};
  }
  if (data.instanceData.send === undefined) {
    data.instanceData.send = {};
  }
  this._data = data;

  this.dataSheetUpdateInterval_ReactWeb = this._data.updateInterval;
}
