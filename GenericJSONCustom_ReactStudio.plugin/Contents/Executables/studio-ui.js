// -- inspector UI --

this.inspectorUIDefinition = [
  {
    "type": "label",
    "text": "Source URL:"
  },
  {
    "type": "textinput",
    "id": "url",
    "actionBinding": "this.onUIChange",
    "multiline": true,
    "height": 80
  },
  {
    "type": "label",
    "text": "Image columns (list of column names, comma separated):"
  },
  {
    "type": "textinput",
    "id": "imageColumns",
    "actionBinding": "this.onUIChange"
  },
  {
    "type": "label",
    "text": "Custom HTTP headers (as a JSON object):"
  },
  {
    "type": "textinput",
    "id": "customHeaders",
    "actionBinding": "this.onUIChange",
    "multiline": true,
    "height": 150
  },
  {
    "type": "label",
    "text": "Method:"
  },
  {
    "type": "multibutton",
    "id": "httpMethod",
    "labels": ["GET", "POST", "DELETE", "PATCH"],
    "actionBinding": "this.onUIChange"
  },
  {
    "type": "checkbox",
    "id": "allowInsecureHttp",
    "actionBinding": "this.onUIChange",
    "label": "Allow insecure HTTP",
    "paddingTop": 6
  },
  {
    "type": "label",
    "text": "Refresh data interval:"
  },
  {
    "type": "numberInput",
    "id": "updateInterval",
    "actionBinding": "this.onUIChange",
    "min": 0, "max": 100, "increment": 1
  }
];

this._uiTextFields = ['url', 'imageColumns', 'customHeaders'];
this._uiNumberFields = ['updateInterval','httpMethod'];
//this._uiColorFields = [];
this._uiCheckboxFields = ['allowInsecureHttp'];

this._accessorForDataKey = function(key) {
  if (this._uiTextFields.indexOf(key) !== -1) return 'text';
  else if (this._uiNumberFields.indexOf(key) !== -1) return 'numberValue';
  else if (this._uiCheckboxFields.indexOf(key) !== -1) return 'checked';
  return null;
}

this.onCreateUI = function() {
  var ui = this.getUI();

  for (var key in this._data) {
    var prop = this._accessorForDataKey(key);
    if (prop){
        ui.getChildById(key)[prop] = this._data[key];
    }
  }
}

this.onUIChange = function(controlId) {
  var ui = this.getUI();
  var prop = this._accessorForDataKey(controlId);
  if (prop) {
    var uiControl = ui.getChildById(controlId);
    var value = uiControl[prop];
    
    if (controlId == 'url' && !this._data.allowInsecureHttp) {
      // force URLs to https
      if (value.indexOf('http://') === 0) {
        value = 'https://'+value.substr(7);
        uiControl[prop] = value;
      }    
    }
    else if (controlId == 'updateInterval') {
      if (value != undefined){
        this.dataSheetUpdateInterval_ReactWeb = value;
      }
    }
    this._data[controlId] = value;
  } else {
    console.log("** no data property found for controlId "+controlId);
  }
}


this.getInspectorUIDefinitionForContext = function(uiContext) {
	switch (uiContext) {
    case 'action:sendData':
      return [
        {
          "type": "label",
          "text": "Endpoint:"
        },
        {
          "type": "textinput",
          "id": "apiEndpoint",
          "multiline": true,
          "height": 40,
          "actionBinding": "this.onUIChange_sendDataAction",
        },
        {
          "type": "dataslot-button",
          "targetTextinputId": "apiEndpoint",
          "template": "$slot('{{DATASLOT_NAME}}')"
        },
        {
          "type": "label",
          "text": "Method:"
        },
        {
          "type": "multibutton",
          "id": "httpMethod",
          "labels": ["GET", "POST", "DELETE", "PATCH"],
          "actionBinding": "this.onUIChange_sendDataAction"
        },
        {
          "type": "label",
          "text": "Body:"
        },
        {
          "type": "textinput",
          "id": "httpBody",
          "multiline": true,
          "height": 80,
          "actionBinding": "this.onUIChange_sendDataAction",
        },
        {
          "type": "dataslot-button",
          "targetTextinputId": "httpBody",
          "template": "this.props.appActions.dataSlots['{{DATASLOT_NAME}}']"
        },
        {
          "type": "label",
          "text": "Save response to data slot:"
        },
        {
          "type": "dataslot-picker",
          "id": "responseDataSlot",
          "actionBinding": "this.onUIChange_sendDataAction"
        },
      ];
  }
}

this.onCreateUIForContext = function(uiContext, instanceId) {
  var ui = this.getUI();
	
	switch (uiContext) {
    case 'action:sendData':
      var instanceData = this._data.instanceData.send[instanceId] || {};
      ui.getChildById("apiEndpoint").text = instanceData["apiEndpoint"] || "";
      ui.getChildById("httpMethod").numberValue = instanceData["httpMethod"];
      ui.getChildById("httpBody").text = instanceData["httpBody"] || ""
        + "{\n"
        + "  // Tip 1 accessing field:\n"
        + "  // \"text\": dataFromElements.fieldName\n"
        + "  // Tip 2 accessing Data slot:\n"
        + "  // \"text\": this.props.appActions.dataSlots['ds_slotName']\n"
        + "}";
      ui.getChildById("responseDataSlot").dataSlotName = instanceData["responseDataSlot"];
      break;
	}
}

this.onUIChange_sendDataAction = function(controlId) {
  var ui = this.getUI();
  
	var instanceId = ""+ui.instanceId;
	if ( !instanceId) { // this shouldn't happen, but check just to be sure
		console.log("** no instance id available for send data action with control id: "+controlId);
		return;
	}
	var instanceData = this._data.instanceData.send[instanceId] || {};
  if (controlId === "apiEndpoint") {
    var val = ui.getChildById("apiEndpoint").text;
    if (val.length > 0 && val.substr(0, 1) !== '/') {
      val = '/'+val;
    }
    instanceData["apiEndpoint"] = val;
  }
  else if (controlId === "httpMethod") {
    instanceData["httpMethod"] = ui.getChildById("httpMethod").numberValue;
  }
  else if (controlId === "httpBody") {
    instanceData["httpBody"] = ui.getChildById("httpBody").text;
  }
  else if (controlId === "responseDataSlot") {
    instanceData["responseDataSlot"] = ui.getChildById("responseDataSlot").dataSlotName;
  }
	this._data.instanceData.send[instanceId] = instanceData;
}

this.cleanReferencesToUIContextInstance = function(instanceId) {
	this._data.instanceData.send[instanceId] = undefined;
}


this.renderIcon = function(canvas) {
  var ctx = canvas.getContext('2d');
  var w = canvas.width;
  var h = canvas.height;
  ctx.save();

  if (this.icon == null) {
    var path = Plugin.getPathForResource("plugin_logo.png");
    this.icon = Plugin.loadImage(path);
  }

  var iconW = this.icon.width;
  var iconH = this.icon.height;
  var aspectScale = Math.min(w/iconW, h/iconH);
  var scale = 0.9 * aspectScale; // add some margin around icon
  iconW *= scale;
  iconH *= scale;
  ctx.drawImage(this.icon, (w-iconW)*0.5, (h-iconH)*0.5, iconW, iconH);

  ctx.restore();
};
